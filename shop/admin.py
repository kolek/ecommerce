from django.contrib import admin
from .models import Category, Product


class CategoryAdmin(admin.ModelAdmin):
  list_display = ['name', 'slug']
  prepopulated_fields = {'slug': ('name',)}
admin.site.register(Category, CategoryAdmin)


class ProductAdmin(admin.ModelAdmin):
  list_display = ['name', 'price', 'stock', 'avilable', 'created']
  list_filter = ['avilable', 'created']
  list_editable = ['price', 'stock', 'avilable']
  prepopulated_fields = {'slug': ('name',)}
admin.site.register(Product, ProductAdmin)