from django.db import models
from django.core.urlresolvers import reverse


class Category(models.Model):

  name = models.CharField(max_length=250, db_index=True)
  slug = models.SlugField(max_length=250, db_index=True, unique=True)

  class Meta:
    verbose_name = "Category"
    verbose_name_plural = "Categories"

  def __str__(self):
    return self.name

  def get_absolute_url(self):
    return reverse('shop:product_list_by_category', args=[self.slug])


class Product(models.Model):
  
  category = models.ForeignKey(Category, related_name='products')
  name = models.CharField(max_length=250, db_index=True)
  slug = models.SlugField(max_length=250, db_index=True)
  image = models.ImageField(upload_to='products', blank=True)
  description = models.TextField(blank=True)
  price = models.DecimalField(max_digits=10, decimal_places=2)
  stock = models.IntegerField()
  avilable = models.BooleanField(default=True)
  created = models.DateTimeField(auto_now_add=True)
  updated = models.DateTimeField(auto_now=True)

  class Meta:
    index_together = (('id', 'slug'),)

  def __str__(self):
    return self.name

  def get_absolute_url(self):
    return reverse('shop:product_detail', args=[self.id, self.slug])